function ss = PEPRMT_final_ss_CO2_Reco(theta,data)

ydata  = data.ydata(:,2);%Reco
gapfill_error = data.ydata(:,3);%daily integral gapfilling error pre-loaded
random_error = data.ydata(:,4);%daily integral random error pre-loaded umol CH4 m-2 d-1
xdata  = data.xdata;

[NEE_mod, S1, S2, ymodel] = PEPRMT_final_sys_CO2_Reco(theta,xdata);


nan_obs=sum(isnan(ydata));
n = length(ydata)-nan_obs;

%simple least squares optimization - following Keenan 2011 and 2012
 ss1 = ((ydata-ymodel')./(random_error+gapfill_error)).^2;
 ss = (nansum(ss1))/n;

 
end
%negative log likelihood function based on Gaussian prob distribution


