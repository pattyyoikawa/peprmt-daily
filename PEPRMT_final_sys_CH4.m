%PEPRMT-DAMM CH4 model 
%Based on Davidson 2012 DAMM model
%parameterized for restored wetlands in the Sacramento-San Joaquin River
%Delta

%Written by Patty Oikawa
%patty.oikawa@gmail.com

%all PEPRMT models use the same input structure (xdata) for CH4 models
%however not all models use all variables in the structure
%all variables are at the daily time step

function  [pulse_emission_total, Plant_flux_net, Hydro_flux,M1,M2, trans2] = PEPRMT_final_sys_CH4(theta,data)
%This is run at daily time step--all variables in daily time step

xdata=data.xdata;

%Constants
R = 8.314;%J K-1 mol-1

%Exogenous Variables
Time_2 =xdata(:,1);% day of year
DOY_disc_2=xdata(:,2);%day of year that starts over every year
TA_2 = xdata(:,3);%Air temperature- measured (C)
WT_2 = xdata(:,4);%water table height (cm) equals 0 when water table at soil surface
%PAR_2 = xdata(:,5);%Photosynthetically active radiation (umol m-2 d-1)
%LAI_2 = xdata(:,6);%Leaf area index
GPP_2 = xdata(:,7);%Modeled GPP - use output from PEPRMT-GPP (gC m-2 day-1) where - = uptake
%GI_2=xdata(:,8);%Greeness index daily average
%Season_drop_2=xdata(:,9);%not used here
%wc_90CI_2=xdata(:,10);
wetland_age_2=xdata(:,11);
%FPAR=xdata(:,12);%not used here
S1_2=xdata(:,13);%Modeled SOC pool - use output from PEPRMT Reco model -cummulative umol m-2
S2_2=xdata(:,14);%Modeled labile C pool - use output from PEPRMT Reco model -cummulative umol m-2
%Sal_2=xdata(:,15);%measured salinity (ppt)

WT_2_adj=(WT_2/100)+1;%makes a new variable where wt=1 at soil surface


%CH4 PARAMETERS
%SOC pool
M_alpha1 = 9e10;% gC m-3 d-1; originally 9e10 umol m-3 s-1
M_ea1 = (theta(1)+65.2)*1000;% parameter in kJ mol-1; multiplied by 1000 = J mol-1
M_km1 =theta(2)+2.2e-10;%g C m-3; originally 1.8e-5 umol m-3 
%Labile C pool
M_alpha2 = 9e11;% gC m-3 d-1; originally 9e11 umol m-3 s-1
M_ea2 = (theta(3)+68)*1000;
M_km2 =theta(4)+1.5e-11;%g C m-3; originally 1.3e-6 umol m-3 

%CH4 oxidation parameters
M_alpha3 = 9e10;% originally 9e10 umol m-3 s-1=7.8e15 umol m-3 d-1
M_ea3 = (theta(5)+59.6)*1000;
M_km3 =theta(6)+253e-5;%

%parameter for hydrodynamic flux
  k=0.04; %gas transfer velocity(m day-1)
%parameter for plant-mediated transport
  Vtrans=0.24;%gas transfer velocity through plants(m d-1)
Oxi_factor=0.35;%percent oxidized during transport

%empirical factors for inhibition of ch4 production when WT drops
  beta1=0.48;
  beta2=-0.18;
  beta3=0.0042;
%empirical factors for decaying inhibition of CH4 production following first
%flooding of wetland
  zeta1=5.1e-6;%%7.4e-6;
  zeta2=0.00058;
  zeta3=0.11;

GPP_2=GPP_2*-1;  
GPPmax=max(GPP_2);%parameter for plant-mediated transport

%Time Invariant
RT = R .* (TA_2 + 274.15);%T in Kelvin-all units cancel out
M_Vmax1 = M_alpha1 .* exp(-M_ea1./RT);%g C m-2 d-1 
M_Vmax2 = M_alpha2 .* exp(-M_ea2./RT);%gC m-2 d-1 
M_Vmax3 = M_alpha3 .* exp(-M_ea3./RT);%gC m-2 d-1 

%preallocating space
S1sol = zeros(1,length(Time_2));
S2sol = zeros(1,length(Time_2));

M1 = zeros(1,length(Time_2));
M2 = zeros(1,length(Time_2));
M1_full = zeros(1,length(Time_2));
M2_full = zeros(1,length(Time_2));
M_full=zeros(1,length(Time_2));
M_percent_reduction=zeros(1,length(Time_2));
M_percent_reduction_2=zeros(1,length(Time_2));

CH4water=zeros(1,length(Time_2));
Hydro_flux=zeros(1,length(Time_2));
Plant_flux=zeros(1,length(Time_2));
Plant_flux_net=zeros(1,length(Time_2));
CH4water_store=zeros(1,length(Time_2));
CH4water_0=zeros(1,length(Time_2));
Oxi_full=zeros(1,length(Time_2));
R_Oxi=zeros(1,length(Time_2));
CH4water_0_2=zeros(1,length(Time_2));
%Vtrans=zeros(1,length(Time_2));
%Oxi_factor=zeros(1,length(Time_2));
trans2=zeros(1,length(Time_2));

for t = 1:length(Time_2)
 %parameter for plant-mediated transport--function of GPP
  trans2(t)=((GPP_2(t)+(GPPmax))/GPPmax)-1;
if trans2(t)<0
    trans2(t)=0;
end
if trans2(t)>1
    trans2(t)=1;
end


 %following Davidson and using multiple eq for diff substrate pool
    M1(t) = M_Vmax1(t) .* S1_2(t) ./(M_km1 + S1_2(t)) ; %gC m2 d-1 Reaction velocity
   
%     if S2_2(t)==0%in winter, no CH4 from Ps C
%         M2(t)=0;
%     else
        M2(t) = M_Vmax2(t) .* S2_2(t) ./(M_km2 + S2_2(t)) ; %gC m2 d-1
%     end
    
    if M1(t)<0
        M1(t)=0;
    end
    if M2(t)<0
        M2(t)=0;
    end
    
%Empirical eq Oikawa for CH4 inhibition when WT falls below soil
%surface--if WT below soil surface any time in 10days previous--CH4
%production reduced
     if t<=20 && WT_2_adj(t)<0.9%original=1
    M_percent_reduction(t)=(beta1*WT_2_adj(t).^2)+(beta2*WT_2_adj(t))+beta3;
   else
    M_percent_reduction(t)=1;
     end
     
     if t>20%20
    Sel=WT_2_adj(t-19:t);
   else
    Sel=5;%placeholder
     end
   if t>20&&nanmin(Sel)<0.9%original=1
    M_percent_reduction(t)=(beta1*WT_2_adj(t).^2)+(beta2*WT_2_adj(t))+beta3;
   else
       if t>20
    M_percent_reduction(t)=1;
       end
   end
   
   if WT_2_adj(t)<0
    M_percent_reduction(t)=0;
   end
    
   if M_percent_reduction(t)<0
    M_percent_reduction(t)=0;
   end
    if M_percent_reduction(t)>1
    M_percent_reduction(t)=1;
   end

%Empirical eq Oikawa for CH4 inhibition following restoration
   if wetland_age_2(t)<2
    M_percent_reduction_2(t)=(zeta1*DOY_disc_2(t).^2)+(zeta2*DOY_disc_2(t))+zeta3;
   else
    M_percent_reduction_2(t)=1;
   end
   if  M_percent_reduction_2(t)>1
        M_percent_reduction_2(t)=1;
   end

   
     
    M1(t) = M1(t)*M_percent_reduction(t) ; %gC m2 d Reaction velocity
    M2(t) = M2(t)*M_percent_reduction(t) ; %gC m2 d
    M1(t) = M1(t)*M_percent_reduction_2(t); %gC m2 d Reaction velocity
    M2(t) = M2(t)*M_percent_reduction_2(t); %gC m2 d

    %S1sol and S2sol are the new SOC and labile pools adjusted for C lost
    %thhru CH4
        S1sol(t) = S1_2(t) - (M1(t));%accounts for depletion of C sources in soil due to Reco and methane production
        S2sol(t) = S2_2(t) - (M2(t));
    
    if S1sol(t)<0
        S1sol(t)=0;
    end
    if S2sol(t)<0
        S2sol(t)=0;
    end
 
   M_full(t)=M1(t)+M2(t);%total CH4 produced at this time step in gC m-3 soil day-1


%NOW COMPUTE CH4 TRANSPORT %%%%%%%%%%%%%%

%make sure WT_2_adj is never negative
if WT_2_adj(t)<0
   WT_2_adj(t)=0;
end

%now start ch4 transport loop
  if t==1
      if WT_2_adj(t)>1 %WT_2_adj = 1 at soil surface
          
      %Methane oxidation is zero when WT above surface
      R_Oxi(t) = 0;
      Oxi_full(t)=0;%umol m-3 d-1
      
      %This assumes you start out the year with no CH4 in water
      %where CH4water_0 is the initial concentration of CH4 in water
      CH4water_0(t)=0;% umol m-3
      CH4water_0_2(t)=0;%CH4 produced in previous time step
      %Only modeling CH4 dissolved in the water that goes down 1 m3 into
      %soil
      CH4water(t)= ((M_full(t)*1)+(CH4water_0(t)*WT_2_adj(t)))/ WT_2_adj(t);%gC per m^3 - concentration in water doesn't change
%multiply CH4 water (mol m-3) by water wt (m) to get to mol m-2, then add
%to M_full (umol m-2 d-1)
      %based on the concentrations in the soil and water, you get hydro and
      %plant-mediated fluxes
      Hydro_flux(t)=k*CH4water(t);  %umol m-3 30min-1; Hydrodynamic flux Poindexter
      Plant_flux(t)=(Vtrans*CH4water(t))*trans2(t);  %umol m-3 30min-1; Plant mediated transport Tian 2001--which just uses CH4 in soil
      Plant_flux_net(t)=Plant_flux(t)*Oxi_factor;  %umol m-3 30min-1; Plant mediated transport after oxidation

      %subtract the moles of methane lost from the pools (soil and
      %water) to the atm
      CH4water_store(t)=CH4water(t)-Hydro_flux(t)-Plant_flux(t); %umol m-3 stored in the system
     
      else  
      %If you start out the year with no water above the surface
      
      %gives CH4 concentration in water which is now less that 1 m33
      CH4water_0(t)=(M_full(t)*1)+(0.00001*WT_2_adj(t))/WT_2_adj(t);%umol per m^3 - concentration in soil and water are the same

      %Methane oxidation turns on when WT falls below the surface
      R_Oxi(t) = M_Vmax3(t) .* CH4water_0(t) ./(M_km3 + CH4water_0(t)) ; %umol m2 day-1 Reaction velocity
      Oxi_full(t)=R_Oxi(t);%gC m-3 d-1
      CH4water(t)=CH4water_0(t)-Oxi_full(t);%now you have less ch4
      if CH4water(t)<0
          CH4water(t)=0;
      end
      CH4water_0_2(t)=0;
      
      %this hydroflux uses a 2nd k parameter which basically inhibits
      %diffusive flux when there is no water above the soil surface
      Hydro_flux(t)=k*CH4water(t);  %gC m-3 d-1; Hydrodynamic flux Poindexter
      Plant_flux(t)=(Vtrans*CH4water(t))*trans2(t);  %umol m-2 30min-1; Plant mediated transport Tian 2001--which just uses CH4 in soil
      Plant_flux_net(t)=Plant_flux(t)*Oxi_factor;  %umol m-2 30min-1; Plant mediated transport after oxidation

      CH4water_store(t)=CH4water(t)-Plant_flux(t)-Hydro_flux(t); %umol m-3 stored in the system
            
      end
  
  else
      if WT_2_adj(t)>1  %if you have water, then the CH4 should mix between the 2 layers and concentrations should be the same in water and soil
      
      %account for any changes in concentration of CH4 due to any change in
      %WT_2_adj height
      CH4water_0(t)=(CH4water_store(t-1) * WT_2_adj(t-1)) / WT_2_adj(t);
      
      %Methane oxidation is zero when WT above surface
      R_Oxi(t) = 0;
      Oxi_full(t)=0;%umol m-3 d-1
      CH4water_0_2(t)=0;
      %Now add the new CH4 produced today to the soil and let it increase
      %in concentration
      CH4water(t)= ((M_full(t)*1)+(CH4water_0(t)*WT_2_adj(t)))/ WT_2_adj(t);%umol per m^3 - concentration in water doesn't change
      
      %again compute fluxes
      Hydro_flux(t)=k*CH4water(t);  %umol m-2 30min-1; Hydrodynamic flux Poindexter
      Plant_flux(t)=(Vtrans*CH4water(t))*trans2(t);  %umol m-2 30min-1; Plant mediated transport Tian 2001--which just uses CH4 in soil
      Plant_flux_net(t)=Plant_flux(t)*Oxi_factor;  %umol m-2 30min-1; Plant mediated transport after oxidation
     
      %subtract the moles of methane lost from the pools (soil and
      %water) to the atm
      CH4water_store(t)=CH4water(t)-Plant_flux(t)-Hydro_flux(t); %umol m-3 stored in the system
          
      else
      %if you don't have WT_2_adj above the soil, then all the CH4 in the water goes to the soil
      %First, account for increased concentration of CH4 in soil now that WT_2_adj has dropped
      CH4water_0(t)=(CH4water_store(t-1) * WT_2_adj(t-1)) / WT_2_adj(t);
    
      %now add new CH4 to this new concentrated pool of CH4 in the soil
      CH4water_0_2(t)= ((M_full(t)*1)+(CH4water_0(t)*WT_2_adj(t)))/ WT_2_adj(t);%gc per m^3 - concentration in water doesn't change

      %Methane oxidation turns on when WT falls below the surface
      R_Oxi(t) = M_Vmax3(t) .* CH4water_0_2(t) ./(M_km3 + CH4water_0_2(t)) ; %gC m2 d Reaction velocity
      Oxi_full(t)=R_Oxi(t);%gC m-3 day-1
      CH4water(t)=CH4water_0_2(t)-Oxi_full(t);%now you have less ch4
      if CH4water(t)<0
          CH4water(t)=0;
      end

      Hydro_flux(t)=k*CH4water(t);  %umol m-3 30min-1; Hydrodynamic flux Poindexter
      Plant_flux(t)=(Vtrans*CH4water(t))*trans2(t);  %umol m-2 30min-1; Plant mediated transport Tian 2001--which just uses CH4 in soil
      Plant_flux_net(t)=Plant_flux(t)*Oxi_factor;  %umol m-2 30min-1; Plant mediated transport after oxidation

      CH4water_store(t)=CH4water(t)-Plant_flux(t)-Hydro_flux(t); %umol m-3 stored in the system
      end
  end
end
pulse_emission_total=Plant_flux_net+Hydro_flux;%gC CH4 m-2 day-1; total CH4 flux to atm

end