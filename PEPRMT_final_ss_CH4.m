function ss = PEPRMT_final_ss_CH4(theta,data)

%time  = data.ydata(:,1);
ydata_m = data.ydata(:,2);%CH4 exchange obs daily integral
gapfill_error_m = data.ydata(:,3);%daily integral gapfilling error pre-loaded
random_error_m = data.ydata(:,4);%daily integral random error pre-loaded umol CH4 m-2 d-1

xdata  = data.xdata;

%run model
ymodel_m = PEPRMT_final_sys_CH4(theta,xdata);


%calculate sample size or # of non nan obs
nan_obs_m=sum(isnan(ymodel_m));
n_m = length(ymodel_m)-nan_obs_m;

%simple least squares optimization - following Keenan 2011 and 2012
 ss1 = ((ydata_m-ymodel_m')./(random_error_m+gapfill_error_m)).^2;
 %Put extra weight on pulses
 ss2=ss1;
 ydata_m_ave=nanmean(ydata_m);
 for i=1:length(ydata_m)
     if ydata_m(i)>4*ydata_m_ave
         ss2(i)=ss1(i)*50;
     else
         ss2(i)=ss1(i);
     end
 end
 ss = (nansum(ss2))/n_m;
 
end
%negative log likelihood function based on Gaussian prob distribution



