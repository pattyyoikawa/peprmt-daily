**PEPRMT Model Code at Daily timestep**

The Peatland Ecosystem Photosynthesis, Respiration and Methane Transport (PEPRMT) model was developed by Patty Oikawa and published in 2017: 
Oikawa, P. Y., et al. "Evaluation of a hierarchy of models reveals importance of substrate limitation for predicting carbon dioxide 
and methane exchange in restored wetlands." Journal of Geophysical Research: Biogeosciences 122.1 (2017): 145-167.

This model code differs from the original published code which ran at the 30min time step
This code is also built to run using LAI or greeness index from remote sensing.

---

## Sys files

The sys files are the core model files. There are 3:

1. PEPRMT_final_sys_CO2_GPP.m predicts CO2 uptake through gross primary productivity
2. PEPRMT_final_sys_CO2_Reco.m predicts CO2 emissions from Ecosystem respiration
3. PEPRMT_final_sys_CH4.m predicts CH4 emission
4. PEPRMT_CO2_example_run.R runs the GPP and Reco functions on the example data
5. PEPRMT_final_sys_CO2_GPP.R predicts CO2 uptake through gross primary productivity
6. PEPRMT_final_sys_CO2_Reco.R predicts CO2 emissions from Ecosystem respiration


---

## Ss files

The ss files are used in Model-Data fusion and calculate the fit between the model and data using simple least squares optimization

1. PEPRMT_final_ss_CO2_GPP.m predicts model-data fit for GPP model 
2. PEPRMT_final_ss_CO2_Reco.m predicts model-data fit for Reco model 
3. PEPRMT_final_ss_CH4.m predicts model-data fit for CH4 model


