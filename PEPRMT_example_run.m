%PEPRMT-Daily run code
%Use this code to run example data through model code and MDF
%find all model code in Bitbucket:
%https://bitbucket.org/pattyyoikawa/peprmt-daily/src/master/

%Author: Patty Oikawa
%Date: October 24 2019

%Example data are called EL_2018_master.mat
load EL_2018_master.mat
ydata=[daily_DOY' daily_gpp_obs daily_co2_gapfilling_error daily_co2_random_error];
xdata=[daily_DOY' daily_DOY' daily_TA' daily_WT'...
    daily_PAR' FPAR' daily_gpp_obs NDVI_placeholder'...
    Season daily_co2_random_error+daily_co2_gapfilling_error...
    daily_wetland_age' FPAR'];
data.xdata = xdata;
data.ydata = ydata;

theta=[2 2 0 0 0];
GPP_mod_EL = PEPRMT_final_sys_CO2_GPP(theta,data);
figure
plot(daily_DOY',daily_gpp_obs,'.')
hold on
plot(daily_DOY',GPP_mod_EL,'.')
legend('obs','mod')
xlabel('DOY 2018')
ylabel('GPP (g C m-2 d-1)')

xdata=[daily_DOY' daily_DOY' daily_TA' daily_WT'...
    daily_PAR' FPAR' GPP_mod_EL' NDVI_placeholder'...
    Season daily_co2_random_error+daily_co2_gapfilling_error...
    daily_wetland_age' FPAR'];
data.xdata = xdata;


theta=[-1 0 -1 0 59280];
[NEE_mod, S1, S2, Reco_mod] = PEPRMT_final_sys_CO2_Reco(theta,data);
figure
plot(daily_DOY,Reco_mod,'.',daily_DOY,daily_er_obs,'.')
legend('mod','obs')
ylabel('Reco  gC m-2 d-1')

figure
subplot(2,1,1)
plot(daily_DOY,S1,'.')
ylabel('S1')
subplot(2,1,2)
plot(daily_DOY,S2,'.')
ylabel('S2')

xdata=[daily_DOY' daily_DOY' daily_TA' daily_WT'...
    daily_PAR' FPAR' GPP_mod_EL' NDVI_placeholder'...
    Season daily_co2_random_error+daily_co2_gapfilling_error...
    daily_wetland_age' FPAR' S1' S2'];
data.xdata = xdata;

theta=[-8 1 -8 1 -8 1];
[CH4_mod, Plant_flux_net, Hydro_flux,M1,M2, trans2] = PEPRMT_final_sys_CH4(theta,data);
figure
plot(daily_DOY,CH4_mod,'.',daily_DOY,daily_ch4_obs,'.')
legend('mod','obs')
ylabel('CH4  gC m-2 d-1')

figure
plot(daily_DOY,Plant_flux_net,'.',daily_DOY,Hydro_flux,'.')
legend('plant','Hydro')
ylabel('CH4  gC m-2 d-1')

output=horzcat(daily_DOY',GPP_mod_EL',Reco_mod',CH4_mod');
csvwrite('EL_2018_master_output.csv',output)
